;;; RTT_revive+_extra.el --- Add extras to revive+
;;
;; Filename: RTT_revive+_extras.el
;; Description: Extend revive+ with extra function to overwrite selected window configuration
;; Author: Robert Templeman
;; Maintainer: Robert Templeman
;; Created: Jun, Sun, 2017 12:38:22 AM
;; Version: 0.1
;; Last-Updated: Jun, Sun, 2017 12:38:22 AM
;;           By: Robert Templeman
;;     Update #: 1
;; URL: www.???.???
;; Keywords: window configuration serialization
;;
;;; Commentary:
;;              extends revive+

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'revive)
(require 'revive+)


(defvar revive-plus:RTT-last-selected-config 1000  ;1000 indicates it's not been set yet
  "The last config selected.")

(defvar revive-plus:RTT-backup-wconf-archive nil
  "A backup config archive to restore if needed.")

(defvar revive-plus:RTT-last-window-buffer-filename-string-hash nil
  "window buffer list after when last preset was invoked.")

;;;###autoload
(defun revive-plus:RTT-wconf-archive-save ()
  "extend revive+ to allow directly overwriting last selected window configuration"
  (interactive)
  (setq temp-config (let ((revive-plus:only-windows t))  ;; ensure to get only windows (no ESCREENs by example)
                      (current-window-configuration-printable)))
  (when (<= (length revive-plus:wconf-archive) revive-plus:RTT-last-selected-config)
      (setq revive-plus:wconf-archive (append revive-plus:wconf-archive revive-plus:wconf-archive))) ;; if list doesnt have enough entries yet, just make some
    
  (setcar (nthcdr revive-plus:RTT-last-selected-config revive-plus:wconf-archive)  temp-config)
  
  (let ((out-num (- (length revive-plus:wconf-archive) revive-plus:wconf-archive-limit)))
    (nbutlast revive-plus:wconf-archive out-num))
  (unless (and (null revive-plus:wconf-archive)
               (not (file-writable-p revive-plus:wconf-archive-file)))
    (with-temp-buffer
      (insert (prin1-to-string revive-plus:wconf-archive))
      (write-region (point-min) (point-max) revive-plus:wconf-archive-file))))



;;;###autoload
(defun revive-plus:RTT-wconf-archive-save-specific (position_to_overwrite)
  "extend revive+ to allow directly overwriting last selected window configuration"
  (interactive)
  (setq temp-config (let ((revive-plus:only-windows t))  ;; ensure to get only windows (no ESCREENs by example)
                      (current-window-configuration-printable)))
;  (when (<= (length revive-plus:wconf-archive) position_to_overwrite)
;      (setq revive-plus:wconf-archive (append revive-plus:wconf-archive revive-plus:wconf-archive))) ;; if list doesnt have enough entries yet, just make some
    
  (setcar (nthcdr position_to_overwrite revive-plus:wconf-archive)  temp-config)
  
  (let ((out-num (- (length revive-plus:wconf-archive) revive-plus:wconf-archive-limit)))
    (nbutlast revive-plus:wconf-archive out-num))
  (unless (and (null revive-plus:wconf-archive)
               (not (file-writable-p revive-plus:wconf-archive-file)))
    (with-temp-buffer
      (insert (prin1-to-string revive-plus:wconf-archive))
      (write-region (point-min) (point-max) revive-plus:wconf-archive-file))))



;;;###autoload
(defun revive:RTT-window-buffer-filename-list ()
  "Return the all shown buffer list.
Each element consists of '(buffer-file-name window-start point)"
  (let ((curw (selected-window))(wlist (revive:window-list)) wblist)
    (save-excursion
      (while wlist
	(select-window (car wlist))
	(set-buffer (window-buffer (car wlist))) ;for Emacs 19
	(setq wblist
	      (append wblist
		      (list (list
			     (if (and (fboundp 'abbreviate-file-name)
				      (buffer-file-name))
				 (abbreviate-file-name (buffer-file-name))
			       (buffer-file-name)))))
	      wlist (cdr wlist)))
      (select-window curw)
      wblist)))



;;;###autoload
(defun revive-plus-extras:RTT-window-buffer-filename-string-hash ()
  "Return the all shown buffer list.
Each element consists of '(buffer-file-name window-start point)"
  (let ((curw (selected-window))(wlist (revive:window-list)) wblist)
    (save-excursion
      (while wlist
        (select-window (car wlist))
        (set-buffer (window-buffer (car wlist))) ;for Emacs 19
        (setq wblist
                (concat wblist
                        (if (and (fboundp 'abbreviate-file-name)
                                 (buffer-file-name))
                            (abbreviate-file-name (buffer-file-name))
                          (buffer-file-name)))
                wlist (cdr wlist)))
      (select-window curw)
      wblist)))



;;;###autoload
(defun revive-plus:RTT-wconf-archive-restore (&optional num)
  "Calls revive+'s revive-plus:wconf-archive-restore after saving the currently selected
window configuration back to the preset.  Thus allowing recall of the exact window
configuration when returning to that preset.  parameter NUM is the preset number"
  (interactive)
  (setq revive-plus:RTT-backup-wconf-archive revive-plus:wconf-archive)  ; make a backup config
                                        ; if window configuration has the same buffers present as
                                        ; the current preset then update the preset
                                        ; this is o that scroll offsets are updated in the preset.
                                        ; Otherwise do not update the saved preset.

; need to update any scrolled windows in the gdb multiple windows frames, but cant save whole frame otherwise if gbd
; isnt loaded the buffer names ger updated incorrectly.
  (if (and (/= num 4) (string= revive-plus:RTT-last-window-buffer-filename-string-hash (revive-plus-extras:RTT-window-buffer-filename-string-hash)))
      (revive-plus:RTT-wconf-archive-save))                          ; save this config in original slot if one was selected earlier
;  (if (< revive-plus:RTT-last-selected-config 1000)
;      (revive-plus:RTT-wconf-archive-save))                          ; save this config in original slot if one was selected earlier
  (setq revive-plus:RTT-last-selected-config num)                        ; save this config num
  (revive-plus:wconf-archive-restore num)
  (setq revive-plus:RTT-last-window-buffer-filename-string-hash (revive-plus-extras:RTT-window-buffer-filename-string-hash)))



;;;   for 35 key emacs keypad
;;;###autoload
(defun revive-plus:start-wconf-archive-extras()
                          "revive+ keybindings and RTTS extra key binding"
  (global-set-key (kbd    "<C-f1>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        0   )))
  (global-set-key (kbd    "<C-f2>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        1   )))
  (global-set-key (kbd    "<C-f3>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        2   )))
  (global-set-key (kbd    "<C-f4>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        3   )))
  (global-set-key (kbd    "<C-f5>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        4   )))
  (global-set-key (kbd    "<C-f6>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        5   )))
  (global-set-key (kbd    "<C-f7>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        6   )))
  (global-set-key (kbd    "<C-f8>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        7   )))
  (global-set-key (kbd    "<C-f9>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        8   )))
; now used for auto hot key action to rfadpidly shut down GDB  (global-set-key (kbd    "<C-f10>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        9   )))
  (global-set-key (kbd    "<C-f11>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        10  )))
  (global-set-key (kbd    "<C-f12>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        11  )))

  (global-set-key (kbd  "<C-M-f1>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        12  )))
  (global-set-key (kbd  "<C-M-f2>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        13  )))
  (global-set-key (kbd  "<C-M-f3>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        14  )))
  (global-set-key (kbd  "<C-M-f4>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        15  )))
  (global-set-key (kbd  "<C-M-f5>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        16  )))
  (global-set-key (kbd  "<C-M-f6>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        17  )))
  (global-set-key (kbd  "<C-M-f7>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        18  )))
  (global-set-key (kbd  "<C-M-f8>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        19  )))
  (global-set-key (kbd  "<C-M-f9>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        20  )))
  (global-set-key (kbd  "<C-M-f10>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        21  )))
  (global-set-key (kbd  "<C-M-f11>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore        22  )))
;  (global-set-key (kbd "<M-f12>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore       9  )))

  (global-set-key (kbd "<S-C-f1>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     0   )))
  (global-set-key (kbd "<S-C-f2>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     1   )))
  (global-set-key (kbd "<S-C-f3>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     2   )))
  (global-set-key (kbd "<S-C-f4>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     3   )))
  (global-set-key (kbd "<S-C-f5>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     4   )))
  (global-set-key (kbd "<S-C-f6>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     5   )))
  (global-set-key (kbd "<S-C-f7>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     6   )))
  (global-set-key (kbd "<S-C-f8>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     7   )))
  (global-set-key (kbd "<S-C-f9>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     8   )))
  (global-set-key (kbd "<S-C-f10>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     9   )))
  (global-set-key (kbd "<S-C-f11>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     10  )))
  (global-set-key (kbd "<S-C-f12>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     11  )))
 
  (global-set-key (kbd "<S-C-M-f1>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     12  )))
  (global-set-key (kbd "<S-C-M-f2>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     13  )))
  (global-set-key (kbd "<S-C-M-f3>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     14  )))
  (global-set-key (kbd "<S-C-M-f4>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     15  )))
  (global-set-key (kbd "<S-C-M-f5>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     16  )))
  (global-set-key (kbd "<S-C-M-f6>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     17  )))
  (global-set-key (kbd "<S-C-M-f7>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     18  )))
  (global-set-key (kbd "<S-C-M-f8>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     19  )))
  (global-set-key (kbd "<S-C-M-f9>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     20  )))
  (global-set-key (kbd "<S-C-M-f10>") #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     21  )))
  (global-set-key (kbd "<S-C-M-f11>") #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific     22  )))
 ; (global-set-key (kbd "<S-C-f12>") #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific       9  )))
  )


;;;   ;; for 10 key emacs keypad
;;;   ;;;###autoload
;;;   (defun revive-plus:start-wconf-archive-extras()
;;;     "revive+ keybindings and RTTS extra key binding"
;;;   ;  (global-unset-key (kbd "<f3>"))
;;;   ;  (global-set-key (kbd "<f3><f3>") #'revive-plus:RTT-wconf-archive-save)
;;;     (global-set-key (kbd "<C-f1>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  0)))
;;;     (global-set-key (kbd "<C-f2>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  1)))
;;;     (global-set-key (kbd "<C-f3>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  2)))
;;;     (global-set-key (kbd "<C-f4>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  3)))
;;;     (global-set-key (kbd "<C-f5>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  4)))
;;;     (global-set-key (kbd "<C-f6>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  5)))
;;;     (global-set-key (kbd "<C-f7>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  6)))
;;;     (global-set-key (kbd "<C-f8>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore  7)))
;;;   ;  (global-set-key (kbd "<C-f9>")    #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore 8)))
;;;   ;  (global-set-key (kbd "<C-f10>")   #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-restore 9)))
;;;   
;;;     (global-set-key (kbd "<S-C-f1>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  0)))
;;;     (global-set-key (kbd "<S-C-f2>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  1)))
;;;     (global-set-key (kbd "<S-C-f3>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  2)))
;;;     (global-set-key (kbd "<S-C-f4>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  3)))
;;;     (global-set-key (kbd "<S-C-f5>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  4)))
;;;     (global-set-key (kbd "<S-C-f6>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  5)))
;;;     (global-set-key (kbd "<S-C-f7>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  6)))
;;;     (global-set-key (kbd "<S-C-f8>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific  7)))
;;;   ;  (global-set-key (kbd "<S-C-f9>")  #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific 8)))
;;;   ;  (global-set-key (kbd "<S-C-f10>") #'(lambda () (interactive)  (revive-plus:RTT-wconf-archive-save-specific 9)))
;;;     )




(provide 'RTT_revive+_extras)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RTT_revive+_extras.el ends here
