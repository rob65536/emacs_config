;; RTTS emacs rc file
;;
;;
;; lookat http://tuhdo.github.io/c-ide.html
;; then
;; http://syamajala.github.io/c-ide.html
;; then maybe
;; https://github.com/Golevka/emacs-clang-complete-async
;;
;;

; c++ style c-c c-o see style at local point

; hide tool bar & menu bar by default
(menu-bar-mode -1)
(tool-bar-mode -1)
;(scroll-bar-mode -1)

(set-face-attribute 'default nil :font "-misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso10646-1" )

(desktop-save-mode 1)
(savehist-mode 1)

;; RTT make it load the current desktop from the current directory
(setq desktop-path '("./"))


(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/")
   t)
  (package-initialize))

; start yasnippet with emacs
(require 'yasnippet)
(yas-global-mode 1)



; RTT setting this to off massively increases scrolling update rate
(setq auto-window-vscroll nil)


;; newline-without-break-of-line
(defun newline-without-break-of-line ()
"1. move to end of the line.
2. insert newline with index"

  (interactive)
  (let ((oldpos (point)))
    (end-of-line)
    (newline-and-indent)))








;                        _                  ___ ____  _____
;    ___ _ __ ___   __ _| | _____          |_ _|  _ \| ____|
;   / __| '_ ` _ \ / _` | |/ / _ \  _____   | || | | |  _|
;  | (__| | | | | | (_| |   <  __/ |_____|  | || |_| | |___
;   \___|_| |_| |_|\__,_|_|\_\___|         |___|____/|_____|
;                                                           


;(require 'flycheck)

;;(require 'cookie)

(set 'cookie-file "~/.emacs.d/yow_file_zippy_pinhead_quotes.txt.gz")


;(global-flycheck-mode)
;(add-hook 'c++-mode-hook (lambda () (setq flycheck-gcc-language-standard "c++11")))

;;(load-file "~/.emacs.d/dogturd/doctor.el")

; get from:
; gcc -v -xc++ /dev/null -

(defun my:cmake_ide_RTT_init ()
  ;;;cmake ide include and symbol include directpoy list
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/local/include/xcb")
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/local/include/portaudio")
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/local/lib/gcc8/include/c++/")
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/local/lib/gcc8/include/c++//x86_64-portbld-freebsd12.0")
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/local/lib/gcc8/include/c++//backward")
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/local/lib/gcc8/gcc/x86_64-portbld-freebsd12.0/8.2.0/include")
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/local/include")
  (add-to-list 'cmake-ide-flags-c++ '"-I/usr/include")
)
; now let's call this function from c/c++ hooks
(add-hook 'c++-mode-hook 'my:cmake_ide_RTT_init)
(add-hook 'c-mode-hook 'my:cmake_ide_RTT_init)



;                          _
;  __ _  ___      ___  ___| |_ _   _ _ __
; / _` |/ __|____/ __|/ _ \ __| | | | '_ \
;| (_| | (_|_____\__ \  __/ |_| |_| | |_) |
; \__,_|\___|    |___/\___|\__|\__,_| .__/
;                                   |_|
;

; start auto-complete with emacs
(require 'auto-complete)
;;; do default config for auto-complete
(require 'auto-complete-config)
(ac-config-default)

; let's define a function which initializes auto-complete-c-headers and gets called for c/c++ hooks
(defun my:ac-c-header-init ()
  ;; auto complete comment to make flycheck happy
  (require 'auto-complete-c-headers)
;  (auto-complete-mode 1)
  (add-to-list 'ac-sources 'ac-source-c-headers)

  (add-to-list 'achead:include-directories '"/usr/local/include/xcb")
  (add-to-list 'achead:include-directories '"/usr/local/include/portaudio")
  (add-to-list 'achead:include-directories '"/usr/local/lib/gcc8/include/c++/")
  (add-to-list 'achead:include-directories '"/usr/local/lib/gcc8/include/c++//x86_64-portbld-freebsd12.0")
  (add-to-list 'achead:include-directories '"/usr/local/lib/gcc8/include/c++//backward")
  (add-to-list 'achead:include-directories '"/usr/local/lib/gcc8/gcc/x86_64-portbld-freebsd12.0/8.2.0/include")
  (add-to-list 'achead:include-directories '"/usr/local/include")
  (add-to-list 'achead:include-directories '"/usr/include")

  (local-set-key (kbd "<return>") 'newline-without-break-of-line)
  (local-set-key (kbd "C-<return>") 'newline)
  )

; now let's call this function from c/c++ hooks
(add-hook 'c++-mode-hook 'my:ac-c-header-init)
(add-hook 'c-mode-hook 'my:ac-c-header-init)

(add-hook 'c++-mode-hook 'imenu-add-menubar-index)
(add-hook 'c-mode-hook 'imenu-add-menubar-index)

(require 'rtags) ;; optional, must have rtags installed

(setq cmake-ide-build-dir "~/rttxcb/.")
;setq cmake-ide-flags-c++ "-I/usr/local/include -I/usr/include -I/home/hich/dir1")
;setq cmake-ide-dir "/home/hich/dir1-release")
(require 'rtags) ;; optional, must have rtags installed
;(setq rtags-rdm-includes "-I/root/rttxcb/RTA --isystem /usr/local/include/xcb --isystem /usr/local/include/portaudio --isystem /usr/local/include --isystem /usr/include --isystem /usr/local/lib/gcc6/gcc/x86_64-portbld-freebsd11.1/6.4.0/include")

 ;(setq rtags-rdm-includes 

(defun ac-rtags-setup ()
  (require 'rtags)
  (setq rtags-autostart-diagnostics t)
  (setq rtags-completion-enabled t)
  (push 'company-rtags company-backends)
)

(add-hook 'c++-mode-hook 'ac-rtags-setup)
(add-hook 'c-mode-hook 'ac-rtags-setup)

;(setq ac-quick-help-delay 0.5)
(setq ac-auto-start 0
      ac-delay 0.4
      ac-quick-help-delay 0.7
      ac-use-fuzzy t
      ac-fuzzy-enable t
      tab-always-indent 'complete ; use 'complete when auto-complete is disabled
      ac-dwim t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-backends
   (quote
    (company-bbdb company-nxml company-css company-eclim company-semantic company-clang company-xcode company-cmake company-capf company-files
                  (company-dabbrev-code company-gtags company-etags company-keywords)
                  company-oddmuse company-dabbrev company-c-headers)))
 '(ecb-options-version "2.50")
 '(ecb-primary-secondary-mouse-buttons (quote mouse-1--mouse-2))
 '(global-auto-revert-mode t)
 '(grep-files-aliases
   (quote
    (("all" . "* .[!.]* ..?*")
     ("el" . "*.el")
     ("ch" . "*.cc *.cxx *.cpp *.C *.CC *.c++ *.h *.hpp *.hxx *.H *.HH *.h++")
     ("c" . "*.cc *.cxx *.cpp *.C *.CC *.c++ *.h *.hpp *.hxx *.H *.HH *.h++")
     ("cc" . "*.cc *.cxx *.cpp *.C *.CC *.c++ *.h *.hpp *.hxx *.H *.HH *.h++")
     ("cchh" . "*.cc *.[ch]xx *.[ch]pp *.[CHh] *.CC *.HH *.[ch]++")
     ("hh" . "*.hxx *.hpp *.[Hh] *.HH *.h++")
     ("h" . "*.h")
     ("l" . "[Cc]hange[Ll]og*")
     ("m" . "[Mm]akefile*")
     ("tex" . "*.tex")
     ("texi" . "*.texi")
     ("asm" . "*.[sS]"))))
 '(gud-tooltip-mode nil)
 '(highlight-symbol-idle-delay 0.5)
 '(imenu-sort-function (quote imenu--sort-by-name))
 '(imenu-use-popup-menu nil)
 '(line-spacing 0)
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
    (nlinum cmake-mode company-rtags ahk-mode thing-cmds smooth-scroll highlight-symbol multiple-cursors chess pov-mode replace-from-region indicators elscreen frame-cmds frame-fns company-c-headers ivy function-args ecb buffer-move cmake-ide company irony bash-completion auto-complete-clang auto-complete-c-headers flycheck iedit)))
 '(savehist-autosave-interval 30)
 '(show-paren-mode t)
 '(speedbar-frame-parameters
   (quote
    ((minibuffer)
     (width . 55)
     (border-width . 0)
     (menu-bar-lines . 0)
     (tool-bar-lines . 0)
     (unsplittable . t)
     (left-fringe . 0))))
 '(speedbar-frame-plist
   (quote
    (minibuffer nil width 55 border-width 0 internal-border-width 0 unsplittable t default-toolbar-visible-p nil has-modeline-p nil menubar-visible-p nil default-gutter-visible-p nil)))
 '(speedbar-sort-tags t)
 '(speedbar-use-images nil)
 '(speedbar-use-imenu-flag t)
 '(tool-bar-mode nil)
 '(tooltip-delay 0.35))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "gray80" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width semi-condensed :foundry "misc" :family "fixed"))))
 '(fixed-pitch ((t nil)))
 '(fixed-pitch-serif ((t nil)))
 '(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face :foreground "dark slate gray" :weight bold))))
 '(font-lock-comment-face ((t (:foreground "dark olive green"))))
 '(font-lock-constant-face ((t (:foreground "light goldenrod"))))
 '(font-lock-doc-face ((t (:inherit font-lock-string-face :foreground "navajo white" :weight semi-light))))
 '(font-lock-keyword-face ((t (:foreground "LightPink1"))))
 '(font-lock-regexp-grouping-backslash ((t (:foreground "white" :weight bold))))
 '(font-lock-regexp-grouping-construct ((t (:inherit bold :foreground "white"))))
 '(font-lock-string-face ((t (:foreground "royal blue"))))
 '(font-lock-type-face ((t (:foreground "SteelBlue1"))))
 '(font-lock-variable-name-face ((t (:foreground "lime green"))))
 '(info-node ((t (:foreground "magenta" :slant italic :weight bold))))
 '(variable-pitch ((t nil))))


; '(font-lock-function-name-face ((t (:foreground "light goldenrod"))))

;font-lock-operator-face
;   _
;  (_)_ __ ___  _ __  _   _
;  | | '__/ _ \| '_ \| | | |
;  | | | | (_) | | | | |_| |
;  |_|_|  \___/|_| |_|\__, |
;                     |___/
;  

(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)

;(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(horizontal-scroll-bar-mode 1)
(window-divider-mode 1)
;;(toggle-truncate-lines 1)
(setq-default truncate-lines t)
;; window layout undo buffer
(winner-mode 1)


; turn on Semantic
;;(semantic-mode 1)

; start auto-complete-clang with emacs
;;(require 'auto-complete-clang)



; emacs development environment mode
;;(global-ede-mode t)

;;(ede-cpp-root-project "rtt_mlssa" :file "/cygdrive/c/RTT_MLSSA/rtt_2.cpp"
;;		      :include-path '("/."))
; you can use system-include-path for setting up the system header file locations.
; turn on automatic reparsing of open buffers in semantic
;(global-semantic-idle-scheduler-mode 1)
(put 'scroll-left 'disabled nil)

;; RTT line  numbers on by default
;(global-linum-mode t)
;(global-linum-mode nil)

(global-nlinum-mode t)

;; RTT truncate  lines by default

(setq truncate-lines t)

;;(add-hook 'diff-mode-hook (lambda () (setq truncate-lines t)))
;;(add-hook 'c++-mode-hook (lambda () (setq truncate-lines t)))
;;(add-hook 'c-mode-hook (lambda () (setq truncate-lines t)))


;; SUBLIMITY SCROLL BAR - doesnt work properly
;;(load-file "~/.emacs.d/sublimityRTT/sublimity.el")
;;(load-file "~/.emacs.d/sublimityRTT/sublimity-scroll.el")
;;(load-file "~/.emacs.d/sublimityRTT/sublimity-map.el")
;;(load-file "~/.emacs.d/sublimityRTT/sublimity-attractive.el")
;;
;;(require 'sublimity)
;;(require 'sublimity-scroll)
;;(require 'sublimity-map) ;; experimental
;;(require 'sublimity-attractive)

;;(setq ac-quick-help-delay 0.2)

(require 'buffer-move) ;; optional, must have rtags installed

(setq yow-file "~/.emacs.d/yow_file_zippy_pinhead_quotes.txt.gz" )

(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'yow 'cookie)



;            _   _                                                            _
;       _ __| |_| |_      ___         ___ ___  _ __ ___  _ __ ___   ___ _ __ | |_
;      | '__| __| __|    / __| _____ / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \ '_ \| __|
;      | |  | |_| |_    | (__ |_____| (_| (_) | | | | | | | | | | |  __/ | | | |_
;      |_|   \__|\__|    \___|       \___\___/|_| |_| |_|_| |_| |_|\___|_| |_|\__|
;                                                                                
;
;; add C // comment to start of line
(fset 'rtt-comment-c-line
      (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([1 47 47 1 down] 0 "%d")) arg)))


(defun rtt-comment-c-line2 ()
  "Cleverer C comment adderer, checks to see if block is marked, if it is, it comments it, if not it comments one line"
  (interactive)
  (if (use-region-p)
      (let*((my-mark (mark)) (my-point (point)))
        (message "commenting active region")
        (if (< my-mark my-point)
            (let* ((temp my-mark))
              (setq my-mark my-point)
              (setq my-point temp)
              ) ; sort so my-mark is at the end of the active region
          )
        (goto-char my-point)
        (while (< (point) my-mark)
          (rtt-comment-c-line)  ; comment out whilst point is still in region
          (setq my-mark (+ my-mark 2))  ;; need to expand test by characters added
          )
        )
    (let*()
      (move-beginning-of-line 1)
      (rtt-comment-c-line)
      )
    )
  )


(defun rtt-uncomment-c-line ()
  "Uncomment a line if it has a C // comment at the start of the line"
  (interactive)
  (let* ((my-line (thing-at-point 'line t)))
    (setq my-new-line my-line)
    (setq match-point (string-match "^ *//" my-line))
    (if (equal match-point nil)
        (message "No C comment")
      (let* ((my-new-line (replace-regexp-in-string "//" "" my-line))
             (new-match-point (string-match "//" my-line)))
        (replace-regexp "//" "" nil (line-beginning-position) (+ (+ (line-beginning-position) new-match-point) 2))
        (message (concat "C comment removed from pos " (number-to-string new-match-point)))
        )
      )
    )
  (c-indent-command)
  (move-beginning-of-line 2)
  )


(defun rtt-uncomment-c-line2 ()
  "Cleverer C comment remover: this checks to see if block is marked, if it is, it uncomments it, if not it uncomments one line"
  (interactive)
  (if (use-region-p)
      (let*((my-mark (mark)) (my-point (point)))
        (message "uncommenting active region")
        (if (< my-mark my-point)
            (let* ((temp my-mark))
              (setq my-mark my-point)
              (setq my-point temp)
              ) ; sort so my-mark is at the end of the active region
          )
        (goto-char my-point)
        (while (< (point) my-mark)
          (rtt-uncomment-c-line)  ; comment out whilst point is still in region
          (setq my-mark (- my-mark 2))  ;; need to expand test by characters added
          )
        )
    (let*()
      (move-beginning-of-line 1)
      (rtt-uncomment-c-line)
      )
    )
  )
;                                                                                                                            
;      _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____
;     |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____|


(global-auto-revert-mode 1)
(setq hscroll-margin 30)
(defun browse-file-directory ()
  "Open the current file's directory however the OS would."
  (interactive)
  (if default-directory
      (browse-url-of-file (expand-file-name default-directory))
    (error "No `default-directory' to open")))
(provide 'init)


;      __                                               _             _
;     / _|_ __ __ _ _ __ ___   ___       ___ ___  _ __ | |_ _ __ ___ | |___
;    | |_| '__/ _` | '_ ` _ \ / _ \     / __/ _ \| '_ \| __| '__/ _ \| / __|
;    |  _| | | (_| | | | | | |  __/    | (_| (_) | | | | |_| | | (_) | \__ \
;    |_| |_|  \__,_|_| |_| |_|\___|     \___\___/|_| |_|\__|_|  \___/|_|___/

(defun RTT-frame-on-this-display()
  "just make frame on display :0.0"
  (interactive)
  (make-frame-on-display ":0.0" '((height . 80) (width . 312) (top . 0) (left . 0)))
;  (run-at-time "1 sec" nil 'maximize-frame)
)


(defun RTT-new-frame-to-the-right()
  "just make frame to the right"
  (interactive)
  (make-frame-on-display ":10.0" '((height . 80) (width . 253) (top . 0) (left . 2290)))
;  (run-at-time "1 sec" nil 'maximize-frame)
)

(setq default-frame-alist '(
                            (width . 256)
                            (height . 50)
                            (right-divider-width . 6)
                            (horizontal-scroll-bars . t)
                            (reverse . t)))

(setq initial-frame-alist '(
                            (width . 313)
                            (height . 80)
                            (bottom-divider-width . 0)
                            (right-divider-width . 6)))

(defun RTT-get-frame-by-title-substring(frame-name-substring)
  "as per title."
  (catch 'RTT-localexit
  (dolist (fr (frame-list))
    (setq RTT-frame-name (substring-no-properties
                          (cdr (assoc 'name (frame-parameters fr)))))
;    (print RTT-frame-name)
    (when (string-match-p (regexp-quote frame-name-substring) RTT-frame-name)
      (throw 'RTT-localexit fr)
             ))
  nil))

(defun RTT-raise-frame-by-title-substring(frame-name-substring)
  (setq frame-found (RTT-get-frame-by-title-substring frame-name-substring))
  (when frame-found
    (select-frame-set-input-focus frame-found)))


;(setq default-frame-alist ((fullscreen . maximized)
; (width . 80)
; (height . 24)
; (right-divider-width . 6)
; (horizontal-scroll-bars . t)
; (reverse . t)))
;(add-to-list 'default-frame-alist '(height . 24))
;(add-to-list 'default-frame-alist '(width . 80))
;      ____ ____  ____    _   _     _
;     / ___|  _ \| __ )  | |_| |__ (_)_ __   __ _ ___
;    | |  _| | | |  _ \  | __| '_ \| | '_ \ / _` / __|
;    | |_| | |_| | |_) | | |_| | | | | | | | (_| \__ \
;     \____|____/|____/   \__|_| |_|_|_| |_|\__, |___/
;                                           |___/     

(setq compilation-read-command nil)

(setq gdb-many-windows 1)

;(defun open-gdb-many-windows-on-display-0.1()
;  "just make frame on display :0.1"
;  (interactive)
;;  (make-frame-on-display ":0.1")
;;  (run-at-time "1 sec" nil 'maximize-frame) ;yo ho ho ho
;  (run-at-time "2 sec" nil 'gdb "-i=mi rttmlssa.exe") ;yo ho ho ho
;)


;(defun RTT-gdb-start-frame ()
;  (interactive)
;;  (select-frame (make-frame))
;  (call-interactively 'gdb))
;
;(global-set-key (kbd "M-f g") 'RTT-gdb-start-frame)



;(defvar gdb-my-history nil "History list for dom-gdb-MYPROG.")
;(defun RTT-dom-gdb-MYPROG ()
;  "Debug MYPROG with `gdb'."
;  (interactive)
;  (let* ((wd "~/rttxcb/")
;         (pr "~/rttxcb/rttmlssa")
;         (dt "~/rttxcb")
;         (arg (concat "gdb --eval-command=run --eval-command=quit -i=mi -cd=" wd " --args " pr " " dt)))
;    (gdb arg)))


(defvar gdb-my-history nil "History list for dom-gdb-MYPROG.")
(defun RTT-dom-gdb-MYPROG ()
  "Debug MYPROG with `gdb'."
  (interactive)
  (let* ((wd "~/rttxcb/")
         (pr "~/rttxcb/rttmlssa")
         (dt "~/rttxcb")
         (arg (concat "gdb -i=mi -cd=" wd " --args " pr " " dt)))
    (gdb arg)))


;(defvar gdb-my-history nil "History list for dom-gdb-MYPROG.")
;(defun RTT-dom-gdb-MYPROG ()
;  "Debug MYPROG with `gdb'."
;  (interactive)
;  (let* ((wd "~/rttxcb/")
;         (pr "~/rttxcb/rttmlssa")
;         (dt "~/rttxcb")
;         (arg (concat "gdb -i=mi -cd=" wd " --args " pr " " dt)))
;    (gdb arg)))


(defun RTT-kill-this-buffer-start-gdb ()
  "kill current buffer & fire off a gdb in it"
  (interactive)
  (progn
    (kill-this-buffer)
    (RTT-dom-gdb-MYPROG)
    ))

(global-unset-key (kbd "M-f"))
(global-set-key (kbd "M-f g") 'RTT-dom-gdb-MYPROG)

; let's define a function which adds semantic as a suggestion backend to auto complete
; and hook this function to c-mode-common-hook
(defun my:add-semantic-to-autocomplete()
  (add-to-list 'ac-sources 'ac-source-semantic)
)
(add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)
;;; init.el ends here

(put 'narrow-to-region 'disabled nil)



;;(setq linum-format "%4d \u2502 ")
;(setq linum-format "%4d")

;                _           _       _           _ _                       
;       ___  ___| | ___  ___| |_    (_)_ __     | (_)_ __  _   _ _ __ ___  
;      / __|/ _ \ |/ _ \/ __| __|   | | '_ \    | | | '_ \| | | | '_ ` _ \ 
;      \__ \  __/ |  __/ (__| |_    | | | | |   | | | | | | |_| | | | | | |
;      |___/\___|_|\___|\___|\__|   |_|_| |_|   |_|_|_| |_|\__,_|_| |_| |_|
;                                                                
;;;; select in line num margin
;(defvar *linum-mdown-line* nil)
;
;(defun line-at-click ()
;	(save-excursion
;		(let ((click-y (cdr (cdr (mouse-position))))
;					(line-move-visual-store line-move-visual))
;			(setq line-move-visual t)
;			(goto-char (window-start))
;			(next-line (1- click-y))
;			(setq line-move-visual line-move-visual-store)
;			;; If you are using tabbar substitute the next line with
;			;; (line-number-at-pos))))
;			(1+ (line-number-at-pos)))))
;
;(defun md-select-linum ()
;	(interactive)
;	(goto-line (line-at-click))
;	(set-mark (point))
;	(setq *linum-mdown-line* (line-number-at-pos)))
;
;(defun mu-select-linum ()
;	(interactive)
;	(when *linum-mdown-line*
;		(let (mu-line)
;			(setq mu-line (line-at-click))
;			(if (> mu-line *linum-mdown-line*)
;					(progn
;						(goto-line *linum-mdown-line*)
;						(set-mark (point))
;						(goto-line mu-line)
;						(end-of-line))
;				(progn
;					(goto-line *linum-mdown-line*)
;					(set-mark (line-end-position))
;					(goto-line mu-line)
;					(beginning-of-line)))
;			(setq *linum-mdown* nil))))
;                                                                                                                            
;      _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____   _____ 
;     |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____| |_____|
                                                                                                                       



;
;(font-lock-add-keywords 'c++-mode
;  `((,(concat
;       "\\<[_a-zA-Z][_a-zA-Z0-9]*\\>"       ; Object identifier
;       "\\s *"                              ; Optional white space
;       "\\(?:\\.\\|->\\)"                   ; Member access
;       "\\s *"                              ; Optional white space
;       "\\<\\([_a-zA-Z][_a-zA-Z0-9]*\\)\\>" ; Member identifier
;       "\\s *"                              ; Optional white space
;       "(")                                 ; Paren for method invocation
;     1 'font-lock-function-name-face t)))

(setq-default c-basic-offset 2)
(setq default-tab-width 2)

;     _   _ _   _          _                    _            _   
;    | |_(_) |_| | ___    | |__   __ _ _ __    | |_ _____  _| |_ 
;    | __| | __| |/ _ \   | '_ \ / _` | '__|   | __/ _ \ \/ / __|
;    | |_| | |_| |  __/   | |_) | (_| | |      | ||  __/>  <| |_ 
;     \__|_|\__|_|\___|   |_.__/ \__,_|_|       \__\___/_/\_\\__|


;  %b -- print buffer name.      %f -- print visited file name.
;  %F -- print frame name.
;  %* -- print %, * or hyphen.   %+ -- print *, % or hyphen.
;        %& is like %*, but ignore read-only-ness.
;        % means buffer is read-only and * means it is modified.
;        For a modified read-only buffer, %* gives % and %+ gives *.
;  %s -- print process status.
;  %i -- print the size of the buffer.
;  %I -- like %i, but use k, M, G, etc., to abbreviate.
;  %p -- print percent of buffer above top of window, or Top, Bot or All.
;  %P -- print percent of buffer above bottom of window, perhaps plus Top,
;        or print Bottom or All.
;  %n -- print Narrow if appropriate.
;  %t -- visited file is text or binary (if OS supports this distinction).
;  %z -- print mnemonics of keyboard, terminal, and buffer coding systems.
;  %Z -- like %z, but including the end-of-line format.
;  %e -- print error message about full memory.
;  %@ -- print @ or hyphen.  @ means that default-directory is on a
;        remote machine.
;  %[ -- print one [ for each recursive editing level.  %] similar.
;  %% -- print %.   %- -- print infinitely many dashes.


;(setq frame-title-format
;      '((:eval (if (buffer-file-name)
;                   (abbreviate-file-name (buffer-file-name))
;                 "%b"))))

			
(setq frame-title-format
  '("emacs%@" (:eval (system-name)) ":       " (:eval (if (buffer-file-name)
                (abbreviate-file-name (buffer-file-name))
                  "%b")) "     %F    [%*]    %i    %p/%P    %Z"))


;
;                                                                         _              _          __  __ 
;      ___ ___  _ __ ___  _ __   __ _ _ __  _   _     _ __ ___   ___   __| | ___     ___| |_ _   _ / _|/ _|
;     / __/ _ \| '_ ` _ \| '_ \ / _` | '_ \| | | |   | '_ ` _ \ / _ \ / _` |/ _ \   / __| __| | | | |_| |_ 
;    | (_| (_) | | | | | | |_) | (_| | | | | |_| |   | | | | | | (_) | (_| |  __/   \__ \ |_| |_| |  _|  _|
;     \___\___/|_| |_| |_| .__/ \__,_|_| |_|\__, |   |_| |_| |_|\___/ \__,_|\___|   |___/\__|\__,_|_| |_|  
;                        |_|                |___/                                                      
;
;; comppnay mode
(add-hook 'after-init-hook 'global-company-mode)
;(add-to-list 'company-backends 'company-c-headers)

; start rtags company autocompletion



;                _           _            _        _   _                             _  _           _                        _   
;      ___      (_)_ __   __| | ___ _ __ | |_ __ _| |_(_) ___  _ __         __ _  __| |(_)_   _ ___| |_ _ __ ___   ___ _ __ | |_ 
;     / __|     | | '_ \ / _` |/ _ \ '_ \| __/ _` | __| |/ _ \| '_ \       / _` |/ _` || | | | / __| __| '_ ` _ \ / _ \ '_ \| __|
;    | (__      | | | | | (_| |  __/ | | | || (_| | |_| | (_) | | | |     | (_| | (_| || | |_| \__ \ |_| | | | | |  __/ | | | |_ 
;     \___|     |_|_| |_|\__,_|\___|_| |_|\__\__,_|\__|_|\___/|_| |_|      \__,_|\__,_|/ |\__,_|___/\__|_| |_| |_|\___|_| |_|\__|
;                                                                                    |__/                                        

;Press ‘C-c C-o’ to see the syntax at point 
(c-set-offset 'case-label 2)


(setq mouse-buffer-menu-mode-mult 0) ; lower number makes it more likely mouse 1 ctrl will sort buffers by mode names



(setq-default indent-tabs-mode nil)  ; always use spaces for indentation


;                               _ _                 _                             __ _                
; ___  __ ___   _____      __ _| | |     __ _ _   _| |_ ___       ___ ___  _ __  / _(_)_ __ _ __ ___  
;/ __|/ _` \ \ / / _ \    / _` | | |    / _` | | | | __/ _ \     / __/ _ \| '_ \| |_| | '__| '_ ` _ \ 
;\__ \ (_| |\ V /  __/   | (_| | | |   | (_| | |_| | || (_) |   | (_| (_) | | | |  _| | |  | | | | | |
;|___/\__,_| \_/ \___|    \__,_|_|_|    \__,_|\__,_|\__\___/     \___\___/|_| |_|_| |_|_|  |_| |_| |_|
;
;
;(defun my-save-some-buffers ()
;(interactive)
;  (save-some-buffers 'no-confirm (lambda ()
;    (cond
;      ((and buffer-file-name (equal buffer-file-name abbrev-file-name)))
;      ((and buffer-file-name (eq major-mode 'latex-mode)))
;      ((and buffer-file-name (eq major-mode 'markdown-mode)))
;      ((and buffer-file-name (eq major-mode 'emacs-lisp-mode)))
;      ((and buffer-file-name (derived-mode-p 'org-mode)))))))
;
;(global-set-key [C-x C-s] 'my-save-some-buffers)
;


;         _   _                             _                 _                 
;    ___ | |_| |__   ___ _ __      _____  _| |_ ___ _ __  ___(_) ___  _ __  ___ 
;   / _ \| __| '_ \ / _ \ '__|    / _ \ \/ / __/ _ \ '_ \/ __| |/ _ \| '_ \/ __|
;  | (_) | |_| | | |  __/ |      |  __/>  <| ||  __/ | | \__ \ | (_) | | | \__ \
;   \___/ \__|_| |_|\___|_|       \___/_/\_\\__\___|_| |_|___/_|\___/|_| |_|___/
;                                                                             
(load-file "~/.emacs.d/extra_elisp_packages/ediff-trees.el")
(load-file "~/.emacs.d/extra_elisp_packages/sunrise-commander.el")
(load-file "~/.emacs.d/extra_elisp_packages/sb-imenu.el")

(require 'sb-imenu)

;;                   _                        _           _                                      _                      ___       ____ _____ _____ 
;;    _ __ _____   _(_)_   _____    __      _(_)_ __   __| | _____      _____     _ __ _____   _(_)_   _____   _       ( _ )     |  _ \_   _|_   _|
;;   | '__/ _ \ \ / / \ \ / / _ \   \ \ /\ / / | '_ \ / _` |/ _ \ \ /\ / / __|   | '__/ _ \ \ / / \ \ / / _ \_| |_     / _ \/\   | |_) || |   | |  
;;   | | |  __/\ V /| |\ V /  __/    \ V  V /| | | | | (_| | (_) \ V  V /\__ \   | | |  __/\ V /| |\ V /  __/_   _|   | (_>  <   |  _ < | |   | |  
;;   |_|  \___| \_/ |_| \_/ \___|     \_/\_/ |_|_| |_|\__,_|\___/ \_/\_/ |___/   |_|  \___| \_/ |_| \_/ \___| |_|      \___/\/   |_| \_\|_|   |_|  
;;                                                                                                                                         
(load-file "~/.emacs.d/extra_elisp_packages/revive.el")
(load-file "~/.emacs.d/extra_elisp_packages/windows.el")
;(win:startup-with-window)
;(define-key ctl-x-map "C" 'see-you-again)
(load-file "~/.emacs.d/extra_elisp_packages/revive-plus-master/revive+.el")
(load-file "~/.emacs.d/extra_elisp_packages/revive-plus-master/RTT_revive+_extras.el")

(require 'windows)
(require 'revive)
(require 'revive+)
(require 'RTT_revive+_extras)
(revive-plus:start-wconf-archive nil)
(revive-plus:start-wconf-archive-extras)



(load-file "~/.emacs.d/extra_elisp_packages/indicate-change-master/indicate-changes.el")

;       _                     _ _                    _                   _       _   
;    __| | ___  ___  ___ _ __(_) |__   ___      __ _| |_     _ __   ___ (_)_ __ | |_ 
;   / _` |/ _ \/ __|/ __| '__| | '_ \ / _ \    / _` | __|   | '_ \ / _ \| | '_ \| __|
;  | (_| |  __/\__ \ (__| |  | | |_) |  __/   | (_| | |_    | |_) | (_) | | | | | |_ 
;   \__,_|\___||___/\___|_|  |_|_.__/ \___|    \__,_|\__|   | .__/ \___/|_|_| |_|\__|
;                                                           |_|                      
;;; describe this point lisp only
(defun describe-foo-at-point ()
  "Show the documentation of the Elisp function and variable near point.
	This checks in turn:
	-- for a function name where point is
	-- for a variable name where point is
	-- for a surrounding function call
	"
  (interactive)
  (let (sym)
    ;; sigh, function-at-point is too clever.  we want only the first half.
    (cond ((setq sym (ignore-errors
                       (with-syntax-table emacs-lisp-mode-syntax-table
                         (save-excursion
                           (or (not (zerop (skip-syntax-backward "_w")))
                               (eq (char-syntax (char-after (point))) ?w)
                               (eq (char-syntax (char-after (point))) ?_)
                               (forward-sexp -1))
                           (skip-chars-forward "`'")
                           (let ((obj (read (current-buffer))))
                             (and (symbolp obj) (fboundp obj) obj))))))
           (describe-function sym))
          ((setq sym (variable-at-point)) (describe-variable sym))
          ;; now let it operate fully -- i.e. also check the
          ;; surrounding sexp for a function call.
          ((setq sym (function-at-point)) (describe-function sym)))))

;  (define-key emacs-lisp-mode-map [(f1)] 'describe-foo-at-point)
;  (define-key emacs-lisp-mode-map [(control f1)] 'describe-function)
;  (define-key emacs-lisp-mode-map [(shift f1)] 'describe-variable)
;   _   _     _                       _                   _       _           _          __  __ 
;  | |_| |__ (_)_ __   __ _      __ _| |_     _ __   ___ (_)_ __ | |_     ___| |_ _   _ / _|/ _|
;  | __| '_ \| | '_ \ / _` |    / _` | __|   | '_ \ / _ \| | '_ \| __|   / __| __| | | | |_| |_ 
;  | |_| | | | | | | | (_| |   | (_| | |_    | |_) | (_) | | | | | |_    \__ \ |_| |_| |  _|  _|
;   \__|_| |_|_|_| |_|\__, |    \__,_|\__|   | .__/ \___/|_|_| |_|\__|   |___/\__|\__,_|_| |_|  
;                     |___/                  |_|                                              
;  

(defun RTT-get-thing-at-point1 ()
  "gets the thing at the point"
  (interactive)
  (setq sym (with-syntax-table emacs-lisp-mode-syntax-table
                         (save-excursion
                           (or (not (zerop (skip-syntax-backward "_w")))
                               (eq (char-syntax (char-after (point))) ?w)
                               (eq (char-syntax (char-after (point))) ?_)
                               (forward-sexp -1))
                           (skip-chars-forward "`'")
                           (let ((obj (read (current-buffer))))
                             (and (symbolp obj) (fboundp obj) obj)))))
  (print sym))


(defun RTT-get-thing-at-point2 ()
  "gets the thing at the point"
  (interactive)
  (setq sym (current-word))
  (print sym))

;   ____ _____ _____                   _                       _          __  __ 
;  |  _ \_   _|_   _|   _ __ ___ _ __ | | __ _  ___ ___    ___| |_ _   _ / _|/ _|
;  | |_) || |   | |    | '__/ _ \ '_ \| |/ _` |/ __/ _ \  / __| __| | | | |_| |_ 
;  |  _ < | |   | |    | | |  __/ |_) | | (_| | (_|  __/  \__ \ |_| |_| |  _|  _|
;  |_| \_\|_|   |_|    |_|  \___| .__/|_|\__,_|\___\___|  |___/\__|\__,_|_| |_|  
;                               |_|                                              



(defun RTT-transfer-secondary-to-primary-region ()
  "If secondary mouse overlay region is highlighted copy this to the primary region return t"
  "if not then return nil"
  (interactive)
  (if (overlay-start mouse-secondary-overlay)
      (progn
        (goto-char (overlay-end mouse-secondary-overlay))
        (exchange-point-and-mark)
        (goto-char (overlay-start mouse-secondary-overlay))
        t)
    nil))



(defun RTT-find-and-replace (to-string)
  "Find and replace wrapper:
  uses primary selection as string to replace, if not set uses emacs current-word
  function replaces with TO-STRING in whole buffer unless secondary mouse selection
  is highlighted, then uses this"
  (interactive "sDo query-replace current word with: ")
  (save-excursion
    (setq from-string (current-word))
    (setq case-fold-search nil)
;    (setq using-primary "false")
;    (setq replacing-in-secondary-region "false")
    (if (use-region-p)
        (progn (setq from-string (buffer-substring (mark) (point)))
;               (setq using-primary "true")
               ))

    (if (RTT-transfer-secondary-to-primary-region)
        (progn
 ;         (setq replacing-in-secondary-region "true")
          (if (> (point) (mark))
              (exchange-point-and-mark))
          (while (search-forward from-string (mark) t)
            (replace-match to-string t)))
      (progn
        (beginning-of-buffer)
        (while (search-forward from-string nil t)
          (replace-match to-string t))))))


(show-paren-mode t) 

;             _ _                 _   _   _                 
;    __ _  __| | |__     ___  ___| |_| |_(_)_ __   __ _ ___ 
;   / _` |/ _` | '_ \   / __|/ _ \ __| __| | '_ \ / _` / __|
;  | (_| | (_| | |_) |  \__ \  __/ |_| |_| | | | | (_| \__ \
;   \__, |\__,_|_.__/   |___/\___|\__|\__|_|_| |_|\__, |___/
;   |___/                                         |___/     

                                      
(defun gud-break-remove ()                                                   
  "Set/clear breakpoint."                                                     
  (interactive)                                                              
  (save-excursion                                                            
    (if (eq (car (fringe-bitmaps-at-pos (point))) 'breakpoint)               
        (gud-remove nil)                                                     
      (gud-break nil))))     


(defun gdb-or-gud-go ()                                                      
  "If gdb isn't running; run gdb, else call gud-go."                         
  (interactive)                                                       
  (if (and gud-comint-buffer                                                 
           (buffer-name gud-comint-buffer)                                   
           (get-buffer-process gud-comint-buffer)                            
           (with-current-buffer gud-comint-buffer (eq gud-minor-mode 'gdba))) ;;if part
      ;;then part 
      (funcall (lambda ()
            (gud-call (if gdb-active-process "continue" "run") "")
;            (speedbar-update-contents)
        )
      )           
    ;;else part
    (funcall(lambda ()
;        (sr-speedbar-close)
        (gdb (gud-query-cmdline 'gdb)))
    )
  );;end:if
  )

(setq compilation-ask-about-save nil)
;(setq compile-command "make -k -j 8") 
(setq compile-command "make -C ~/rttxcb -k -j 8") 
       
;   _____ ____ ____     ____  _          __  __ 
;  | ____/ ___| __ )   / ___|| |_ _   _ / _|/ _|
;  |  _|| |   |  _ \   \___ \| __| | | | |_| |_ 
;  | |__| |___| |_) |   ___) | |_| |_| |  _|  _|
;  |_____\____|____/   |____/ \__|\__,_|_| |_|  
;                                               

(defun RTT-send-current-buffer-to-ecb ()
  "sends current buffer filename to ECB"
  (interactive)
  (ecb-set-selected-source (buffer-file-name) nil nil))


;   _  __                                
;  | |/ /___ _   _ _ __ ___   __ _ _ __  
;  | ' // _ \ | | | '_ ` _ \ / _` | '_ \ 
;  | . \  __/ |_| | | | | | | (_| | |_) |
;  |_|\_\___|\__, |_| |_| |_|\__,_| .__/ 
;            |___/                |_|    

;(global-set-key (kbd "<f7>") 'RTT-send-current-buffer-to-ecb)

(global-unset-key (kbd "M-f x"))
(global-set-key (kbd "M-f x") 'delete-frame)
;(global-unset-key (kbd "M-C-q"))
;(global-set-key (kbd "M-w") 'delete-frame)

(global-unset-key (kbd "<f3>"))
(global-set-key (kbd "<f3>") '(lambda () (interactive) (save-some-buffers t)))

(global-set-key (kbd "C-z") 'undo)    
;(global-set-key (kbd "<f8>") 'undo)    ; F8 undo RTT
(global-set-key (kbd "M-s") 'other-window)    ; m-s other

; Fix iedit bug in Mac and cygwin
;(define-key global-map (kbd "C-c ;") 'iedit-mode)

(global-unset-key (kbd "M-g"))
;(global-unset-key (kbd "M-f"))

(global-set-key (kbd "M-g") 'rtags-find-symbol-at-point)
(global-set-key (kbd "M-f r") 'rtags-find-references-at-point)
(global-set-key (kbd "M-f v") 'rtags-find-virtuals-at-point)

(global-unset-key (kbd "M-j"))
(global-set-key (kbd "M-j") 'browse-file-directory)
(global-set-key (kbd "M-f j") 'browse-file-directory)

;(global-set-key (kbd "M-f g") 'gdb)

(global-unset-key (kbd "M-d"))
(global-set-key (kbd "M-d") 'ff-find-other-file)
(global-set-key (kbd "M-c") 'delete-frame)

(global-set-key (kbd "M-f <up>")     'buf-move-up)
(global-set-key (kbd "M-f <down>")   'buf-move-down)
(global-set-key (kbd "M-f <left>")   'buf-move-left)
(global-set-key (kbd "M-f <right>")  'buf-move-right)
;;(global-set-key (kbd "C-S-right")  'buf-move-right)
;;(global-set-key (kbd "C-S-left")   'buf-move-left)
;;(global-set-key (kbd "C-S-up")     'buf-move-up)
;;(global-set-key (kbd "C-S-down")   'buf-move-down)

;(global-set-key (kbd "M-4")     'isearch-forward-symbol-at-point)

;; fiddle to remap C-[ from ESC to a useful key
(define-key input-decode-map [?\C-\[] (kbd "<C-[>"))
(global-set-key (kbd "<C-[>") 'rtt-comment-c-line2)
(global-set-key (kbd "C-]") 'rtt-uncomment-c-line2)

(global-set-key (kbd "C-o") 'RTT-new-frame-to-the-right)
(global-set-key (kbd "S-C-o") 'RTT-frame-on-this-display)

;(global-unset-key (kbd "M-g"))
;(global-set-key (kbd "M-g k") 'open-gdb-many-windows-on-display-0.1)

;(global-set-key (kbd "<left-margin> <down-mouse-1>") 'md-select-linum)
;(global-set-key (kbd "<left-margin> <mouse-1>") 'mu-select-linum)
;(global-set-key (kbd "<left-margin> <drag-mouse-1>") 'mu-select-linum)

(global-set-key (kbd "<f1>") 'describe-foo-at-point)


(global-set-key (kbd "C-l" ) 'kmacro-start-macro-or-insert-counter)
(global-set-key (kbd "<f2>") 'kmacro-end-or-call-macro)
(global-set-key (kbd "<f4>") 'RTT-find-and-replace)
(global-set-key (kbd "<f7>") 'rgrep)
(global-set-key (kbd "S-C-x") 'kill-this-buffer)
;(global-set-key (kbd "S-C-M-x") 'RTT-kill-this-buffer-start-gdb)

(global-set-key (kbd "C-M-r") 'rtags-rename-symbol)


;(global-unset-key (kbd "M-d"))

;(global-set-key (kbd "C-<left>") 'move-beginning-of-line)
;(global-set-key (kbd "C-<right>") 'move-end-of-line)

; reset cut n paste kys to be half sensible
(global-set-key (kbd "C-q") 'yank)
;(global-unset-key (kbd "C-w"))
;(global-unset-key (kbd "M-w"))
;(global-set-key (kbd "C-w") 'kill-ring-save)
;(global-set-key (kbd "M-w") 'kill-region)



;(global-set-key (kbd "M-SPC") 'mc/edit-lines)
(global-set-key (kbd "M-SPC") 'set-rectangular-region-anchor)
(global-set-key (kbd "M-<down>") 'mc/mark-next-like-this)

; catch windows Alt-F (S)ave accidental presses & save file
(global-set-key (kbd "M-f s") 'speedbar)

;(global-unset-key (kbd "C-m"))
;(global-unset-key (kbd "S-C m"))
(global-set-key (kbd "M-i") 'imenu)

(global-set-key (kbd "<drag-mouse-8>") 'imenu)
(global-set-key (kbd "<drag-mouse-9>") 'imenu)
(global-set-key (kbd "<mouse-8>") 'imenu)
(global-set-key (kbd "<mouse-9>") 'imenu)


(global-set-key (kbd "M-r") 'gdb-restore-windows)
(global-set-key (kbd "M-f a") 'mc/vertical-align-with-space)


; ____            _                            _____                       __  __      _             
;|  _ \ __ _  ___| | ____ _  __ _  ___  ___   |  ___| __ ___  _ __ ___    |  \/  | ___| |_ __   __ _ 
;| |_) / _` |/ __| |/ / _` |/ _` |/ _ \/ __|  | |_ | '__/ _ \| '_ ` _ \   | |\/| |/ _ \ | '_ \ / _` |
;|  __/ (_| | (__|   < (_| | (_| |  __/\__ \  |  _|| | | (_) | | | | | |  | |  | |  __/ | |_) | (_| |
;|_|   \__,_|\___|_|\_\__,_|\__, |\___||___/  |_|  |_|  \___/|_| |_| |_|  |_|  |_|\___|_| .__/ \__,_|
;                           |___/                                                       |_|          
; packages installed manually from melpa
;  zygospore          20140703.152  available  melpa      reversible C-x 1 (delete-other-windows)
;  zzz-to-char        20170519.335  available  melpa      Fancy version of `zap-to-char' command
;  auto-complete-c... 20150911.2023 installed             An auto-complete source for C/C++ header files
;  auto-complete-c... 20140409.52   installed             Auto Completion source for clang for GNU Emacs
;  bash-completion    20150514.728  installed             BASH completion for the shell buffer
;  buffer-move        20160615.1103 installed             easily swap buffers
;  chess              2.0.4         installed             Play chess in GNU Emacs
;  cmake-ide          20170316.643  installed             Calls CMake to find out include paths and other compiler flags
;  company            0.9.2         installed             Modular text completion framework
;  company-c-headers  20150801.901  installed             Company mode backend for C/C++ header files
;  ecb                20160101.933  installed             a code browser for Emacs
;  elscreen           20160613.251  installed             Emacs window session manager
;  flycheck           20170309.145  installed             On-the-fly syntax checking
;  frame-cmds         20170222.1758 installed             Frame and window commands (interactive functions).
;  frame-fns          20170222.1759 installed             Non-interactive frame and window functions.
;  iedit              20161228.2111 installed             Edit multiple regions in the same way simultaneously.
;  indicators         20161211.326  installed             Display the buffer relative location of line in the fringe.
;  irony              20170518.1024 installed             C/C++ minor mode powered by libclang
;  multiple-cursors   20170713.1847 installed             Multiple cursors for Emacs.
;  pov-mode           20161114.2343 installed             Major mode for editing POV-Ray scene files.
;  replace-from-re... 20170227.1516 installed             Replace commands whose query is from region
;  rtags              20170303.1422 installed             A front-end for rtags
;  yasnippet          20170226.1638 installed             Yet another snippet extension for Emacs.
;  auto-complete      20170124.1845 dependency            Auto Completion for GNU Emacs
;  dash               20170207.2056 dependency            A modern list library for Emacs
;  epl                20150517.433  dependency            Emacs Package Library
;  levenshtein        20051013.1056 dependency            Edit distance between two strings.
;  pkg-info           20150517.443  dependency            Information about packages
;  popup              20160709.729  dependency            Visual Popup User Interface
;  allout             2.3           built-in              extensive outline mode for use alone and with other modes
;  allout-widgets     1.0           built-in              Visually highlight allout outline structure.
;  ansi-color         3.4.2         built-in              translate ANSI escape sequences into faces
;  antlr-mode         2.2.3         built-in              major mode for ANTLR grammar files
;  artist             1.2.6         built-in              draw ascii graphics with your mouse


;                     _      _             _                 _                           _   
; _ __   ___  ___  __| |    | |_ ___      (_)_ __ ___  _ __ | | ___ _ __ ___   ___ _ __ | |_ 
;| '_ \ / _ \/ _ \/ _` |    | __/ _ \     | | '_ ` _ \| '_ \| |/ _ \ '_ ` _ \ / _ \ '_ \| __|
;| | | |  __/  __/ (_| |    | || (_) |    | | | | | | | |_) | |  __/ | | | | |  __/ | | | |_ 
;|_| |_|\___|\___|\__,_|     \__\___/     |_|_| |_| |_| .__/|_|\___|_| |_| |_|\___|_| |_|\__|
;                                                     |_|                                    
                                        ;
; fix winner mode to save from different frames
;Find symbol under cursor in files to another frame
;spedbar like dropdown menu of functions and structs in current C source file when click C-S-mouse1

; DONE F10 F11 like keys for GDB setupc
;DONE:  fast F4 like search anf replace dialog to replace symbol under cursor
;DONE: restore window layout to small set of defaults

(setq hlt-auto-faces-flag t)

;(server-start)

(setq-default display-buffer-reuse-frames t)

;; cursor blinking rate
;(defvar blink-cursor-interval-visible 0.08)
;(defvar blink-cursor-interval-invisible 0.03)
;
;(defadvice internal-show-cursor (before unsymmetric-blink-cursor-interval)
;  (when blink-cursor-timer
;    (setf (timer--repeat-delay blink-cursor-timer)
;          (if (internal-show-cursor-p)
;              blink-cursor-interval-visible
;            blink-cursor-interval-invisible))))
;(ad-activate 'internal-show-cursor)


; _            __  __                        _           _                             _           _   _             
;| |__  _   _ / _|/ _| ___ _ __    __      _(_)_ __   __| | _____      __     ___  ___| | ___  ___| |_(_) ___  _ __  
;| '_ \| | | | |_| |_ / _ \ '__|___\ \ /\ / / | '_ \ / _` |/ _ \ \ /\ / /____/ __|/ _ \ |/ _ \/ __| __| |/ _ \| '_ \ 
;| |_) | |_| |  _|  _|  __/ | |_____\ V  V /| | | | | (_| | (_) \ V  V /_____\__ \  __/ |  __/ (__| |_| | (_) | | | |
;|_.__/ \__,_|_| |_|  \___|_|        \_/\_/ |_|_| |_|\__,_|\___/ \_/\_/      |___/\___|_|\___|\___|\__|_|\___/|_| |_|
;   

(setq pop-up-frames t)


(defadvice pop-to-buffer (before cancel-other-window first)
  (ad-set-arg 1 nil))

(ad-activate 'pop-to-buffer)

;; Toggle window dedication
(defun toggle-window-dedicated ()
  "Toggle whether the current active window is dedicated or not"
  (interactive)
  (message
   (if (let (window (get-buffer-window (current-buffer)))
         (set-window-dedicated-p window 
                                 (not (window-dedicated-p window))))
       "Window '%s' is dedicated"
     "Window '%s' is normal")
   (current-buffer)))

;; Press [pause] key in each window you want to "freeze"
(global-set-key [pause] 'toggle-window-dedicated)


(require 'highlight-symbol)
;(defcustom RTT-highlight-symbol-ignore-list '()
;  "List of regexp rules that specifies what symbols should not be highlighted."
;  :type '(repeat string)
;  :group 'highlight-symbol)
;
;(defconst RTT-highlight-symbol-border-pattern
;  (if (>= emacs-major-version 22) '("\\_<" . "\\_>") '("\\<" . "\\>")))
;
;
;(defun RTT-highlight-symbol-get-symbol ()
;  "Return a regular expression identifying the symbol at point."
;  (let ((symbol (thing-at-point 'symbol)))
;    (when (and symbol
;               (not (member 0 (mapcar
;                               (lambda (e) (string-match e symbol))
;                               RTT-highlight-symbol-ignore-list))))
;      (concat (car RTT-highlight-symbol-border-pattern)
;              (regexp-quote symbol)
;              (cdr RTT-highlight-symbol-border-pattern)))))


(defvar RTT-last-symbol nil)

(defun RTT-highlight-symbol-next ()
  "Jump to the next location of the symbol at point within the buffer."
  (interactive)
  (progn
    (setq RTT-last-symbol (highlight-symbol-get-symbol))
    (highlight-symbol-jump 1)))


(defun RTT-highlight-symbol-prev ()
  "Jump to the previous location of the symbol at point within the buffer."
  (interactive)
  (progn
    (setq RTT-last-symbol (highlight-symbol-get-symbol))
    (highlight-symbol-jump -1)))


(defun RTT-highlight-last-symbol-next()
  "Jump to the next location of the symbol last used for RTT-highlight-symbol-next within the buffer."
  (interactive)
  (re-search-forward RTT-last-symbol nil t 1))


(defun RTT-highlight-last-symbol-prev()
  "Jump to the previous location of the symbol last used for RTT-highlight-symbol-prev within the buffer."
  (interactive)
  (re-search-forward RTT-last-symbol nil t -1))

(defun unset-default-c-mode-keys ()
  "Jump to the previous location of the symbol at point within the buffer."
  (interactive)
  (progn
    (local-unset-key "\M-e")
    ))


(add-hook 'c++-mode-hook 'unset-default-c-mode-keys)
(add-hook 'c-mode-hook 'unset-default-c-mode-keys)

(global-unset-key (kbd "M-e"))
(global-set-key (kbd "M-e")     'RTT-highlight-last-symbol-prev)
(global-set-key (kbd "M-r")     'RTT-highlight-last-symbol-next)
(global-set-key (kbd "M-3")     'RTT-highlight-symbol-prev)
(global-set-key (kbd "M-4")     'RTT-highlight-symbol-next)

;(global-unset-key (kbd "M-3"))
;(global-set-key (kbd "S-M-e")     'eval-last-sexp)

(setq compilation-read-command nil)

;   
;                              _ _       _   _                           _      _                       _             
;     ___ ___  _ __ ___  _ __ (_) | __ _| |_(_) ___  _ __             __| | ___| |__  _   _  __ _  __ _(_)_ __   __ _ 
;    / __/ _ \| '_ ` _ \| '_ \| | |/ _` | __| |/ _ \| '_ \   _____   / _` |/ _ \ '_ \| | | |/ _` |/ _` | | '_ \ / _` |
;   | (_| (_) | | | | | | |_) | | | (_| | |_| | (_) | | | | |_____| | (_| |  __/ |_) | |_| | (_| | (_| | | | | | (_| |
;    \___\___/|_| |_| |_| .__/|_|_|\__,_|\__|_|\___/|_| |_|          \__,_|\___|_.__/ \__,_|\__, |\__, |_|_| |_|\__, |
;                       |_|                                                                 |___/ |___/         |___/ 
;   

;
;(defun my-compilation-finish-function (buffer desc)
;  (message "Buffer %s: %s" buffer desc))
;(add-hook 'compilation-finish-functions 'my-compilation-finish-function)
;


;(setq compilation-finish-function
;(lambda (buf str)
;    (if (string-match ".*finished.*" str)
;        ;;no errors, make the compilation window go away in a few seconds
;        (gud-go))))



;(setq compilation-finish-function
;(lambda (buf str)
;    (if (null (string-match ".*exited abnormally.*" str))
;        (progn
;          (funcall (lambda ()
;                     (gud-call (if gdb-active-process "continue" "run") "")))))))
;;          (run-at-time
;;           "1 sec" nil 'delete-windows-on
;;           (get-buffer-create "*compilation*"))
;;          (message "No Compilation Errors!")))))



(defvar RTT-last-compilation-success nil)


(defun RTT-new-compile-function()
  "if last compile was success just restart the debugger"
  (interactive)
  (if RTT-last-compilation-success
      (gud-call "r")
    (compile)
    ))

(defun notify-compilation-result(buffer msg)
  "Notify that the compilation is finished,
close the *compilation* buffer if the compilation is successful,
and set the focus back to Emacs frame"
  (if (string-match "^finished" msg)
      (progn
        (setq RTT-last-compilation-success 't)
                                        ;     (delete-windows-on buffer)  ; RTT close compialtion bvuffer window
        ;(gud-call (if gdb-active-process "continue" "run") "")
        ) ; RTT: this is basically gud-go
    (progn
      (setq RTT-last-compilation-success 'f)
      (tooltip-show "\n Compilation Failed :-( \n "))
    )
;  (setq current-frame (car (car (cdr (current-frame-configuration)))))
;  (select-frame-set-input-focus current-frame)
  )


(defun old-notify-compilation-result(buffer msg)
  "Notify that the compilation is finished,
close the *compilation* buffer if the compilation is successful,
and set the focus back to Emacs frame"
  (if (string-match "^finished" msg)
      (progn
        (setq RTT-last-compilation-success 't)
                                        ;     (delete-windows-on buffer)  ; RTT close compialtion bvuffer window
        (gud-call (if gdb-active-process "continue" "run") "")) ; RTT: this is basically gud-go
    (progn
      (setq RTT-last-compilation-success 'f)
      (tooltip-show "\n Compilation Failed :-( \n "))
    )
;  (setq current-frame (car (car (cdr (current-frame-configuration)))))
;  (select-frame-set-input-focus current-frame)
  )


(add-to-list 'compilation-finish-functions
             'old-notify-compilation-result)


(defun my-new-compilation-hook ()
  "RTTs start of compile hook."
  (if (not (get-buffer-window "*compilation*" t))
    (progn
;      (setq f (make-frame '((height . 80) (width . 253) (top . 0) (left . 2290))))
      (setq f (make-frame '((height . 50) (width . 200) (top . 0) (left . 2490))))
      (select-frame f)
      (switch-to-buffer "*compilation*")
      (RTT-raise-frame-by-title-substring "compilation")
      )
    )
  )

(defun my-new-compilation-hook2 ()
  "RTTs start of compile hook."
  (RTT-raise-frame-by-title-substring "compilation")
  )


;(add-hook 'compilation-mode-hook 'my-new-compilation-hook2)

;  
;  F4 	  	Go to the next compilation error message and the corresponding source code (Emacs function next-error).
;  Shift-F4 	  	Go to the previous compilation error message and the corresponding source code (Emacs function previous-error).
;  Debugging
;  
;  F9 	  	Set a breakpoint at the source line that the point is on (Emacs function gud-break).
;  Shift-F9 	  	Delete the breakpoint(s) on the current source line (Emacs function gud-remove).
;  Ctrl-F9 	  	Set a temporary breakpoint on the current source line (Emacs function gud-tbreak).
;  F10 	  	Execute current source line (Emacs function gud-next).
;  Shift-F10 	  	Continue execution (Emacs function gud-cont).
;  F11 	  	Execute until another source line is reached (Emacs function gud-step).
;  Shift-F11 	  	Run until the selected stack frame returns (Emacs function gud-finish).
;  F12 	  	Move down the stack frames (Emacs function gud-down).
;  Shift-F12 	  	Move up the stack frames (Emacs function gud-up). 
;  (load-file "~/.emacs.d/extra_elisp_packages/gdb-settings/gdb-settings.el")
;       
;  (require 'gdb-settings)
;  
;  (gdb-key-settings)

(global-set-key (kbd "<f6>") 'next-error)
(global-set-key (kbd "<S-f2>") 'previous-error)

(global-unset-key (kbd "<f5>"))
(global-set-key (kbd "<f5>") 'compile)

;  (global-set-key keymap [f7] 'devenv-emulation-make-active-project)
;  (global-set-key keymap [S-f7] 'devenv-emulation-clean-active-project)
;  (global-set-key keymap [C-f7] 'devenv-emulation-compile-buffer-file-to-object-file)

(global-set-key (kbd "<f9>") 'gud-break-remove)
(global-set-key (kbd "<S-f9>") 'gud-remove)
;  (global-set-key (kbd "<C-f9>") 'gud-tbreak)
(global-set-key (kbd "<f10>") 'gud-next)
(global-set-key (kbd "<S-f10>") 'gud-cont)
(global-set-key (kbd "<f11>") 'gud-step)
(global-set-key (kbd "<S-f11>") 'gud-finish)
(global-set-key (kbd "<f12>") 'gud-down)
(global-set-key (kbd "<S-f12>") 'gud-up)
;  (global-set-key keymap [?\C-c ?c] 'devenv-emulation-switch-to-compilation-buffer)
;  (global-set-key keymap [?\C-c ?d] 'devenv-emulation-switch-to-gud-buffer)
;  (global-set-key keymap [?\C-c ?m] 'devenv-emulation-make-target)





    ;; scroll one line at a time (less "jumpy" than defaults)
    
(setq mouse-wheel-scroll-amount '(4 ((shift) . 8) ((control) . 16)))
 
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
 
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
    
(setq scroll-step 1) ;; keyboard scroll one line at a time

(setq scroll-margin 8
      scroll-conservatively 0
      scroll-up-aggressively 0.001
      scroll-down-aggressively 0.001)
(setq-default scroll-up-aggressively 0.001
              scroll-down-aggressively 0.001)

;(require 'thing-cmds)
                                        ;(thgcmd-bind-keys)

;              _       _        _     _     _                   
;  _ __   ___ (_)_ __ | |_     | |__ (_)___| |_ ___  _ __ _   _ 
; | '_ \ / _ \| | '_ \| __|____| '_ \| / __| __/ _ \| '__| | | |
; | |_) | (_) | | | | | ||_____| | | | \__ \ || (_) | |  | |_| |
; | .__/ \___/|_|_| |_|\__|    |_| |_|_|___/\__\___/|_|   \__, |
; |_|                                                     |___/ 


(defvar point-undo-ring-length 2000)

(defvar point-undo-ring (make-ring point-undo-ring-length))
(make-variable-buffer-local 'point-undo-ring)

(defvar point-redo-ring (make-ring point-undo-ring-length))
(make-variable-buffer-local 'point-redo-ring)

(defun point-undo-pre-command-hook ()
  "Save positions before command."
  (unless (or (eq this-command 'point-undo)
              (eq this-command 'point-redo))
    (let ((line (line-number-at-pos)))
      (when (eq line (cdr (nth 0 (ring-elements point-undo-ring))))
        (ring-remove point-undo-ring 0))
      (ring-insert point-undo-ring (cons (point) line))
      (setq point-redo-ring (make-ring point-undo-ring-length)))))

(add-hook 'pre-command-hook 'point-undo-pre-command-hook)

(defun point-undo-doit (ring1 ring2)
  "ring1, ring2 = {point-undo-ring, point-redo-ring}"
  (condition-case nil
      (progn
        (goto-char (car (nth 0 (ring-elements ring1)))) 
        (ring-insert ring2 (ring-remove ring1 0)))
    (error nil)))

(defun point-undo ()
  "Undo position."
  (interactive)
  (point-undo-doit point-undo-ring point-redo-ring))

(defun point-redo ()
  "Redo position."
  (interactive)
  (when (or (eq last-command 'point-undo)
            (eq last-command 'point-redo))
    (point-undo-doit point-redo-ring point-undo-ring)))
;(define-key global-map [f9]  'point-undo)
;(define-key global-map [f10] 'point-redo)
;(global-set-key (kbd "<C-f9>")  'point-undo)
;(global-set-key (kbd "<C-f10>") 'point-redo)

;             _                    _                  
;   _ __ ___ (_)___  ___          | | _____ _   _ ___ 
;  | '_ ` _ \| / __|/ __|  _____  | |/ / _ \ | | / __|
;  | | | | | | \__ \ (__  |_____| |   <  __/ |_| \__ \
;  |_| |_| |_|_|___/\___|         |_|\_\___|\__, |___/
;                                           |___/     

; control-delete to kill line & stick last line on kill ring
(fset 'kill-and-copy-line
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([1 S-down 23] 0 "%d")) arg)))

(global-unset-key (kbd "<C-delete>"))
(global-set-key (kbd "<C-delete>")  'kill-and-copy-line)


;phuck2 (defface font-lock-operator-face
;phuck2   '((((class color) (min-colors 88) (background light))
;phuck2      :background "darkseagreen2")
;phuck2     (t :inverse-video t))
;phuck2   "Basic face for highlighting."
;phuck2   :group 'basic-faces)

;(defface font-lock-operator-face "Basic face for highlighting." :group 'basic-faces)
;phuck   ;;       * the name of our face *
;phuck   (defface font-lock-operator-face
;phuck     '((((class color)
;phuck          :background "darkseagreen2")))
;phuck     "Basic face for highlighting."
;phuck     :group 'basic-faces)
;phuck   
   ;; You'll have a hard time missing these colors
;phuck2    (set-face-foreground 'font-lock-operator-face "red")
;phuck2    (set-face-background 'font-lock-operator-face "blue")
;phuck2    
;phuck2    (font-lock-add-keywords 'c++-mode
;phuck2      '(("\\(~^&\|!<>:=,.\\+*/%-]\\)" 0 'font-lock-operator-face)))


;;(defface font-lock-c++-operator-face
;;  '((((class color))
;;     (:inherit font-lock-builtin-face :bold t)))
;;  "Font Lock mode face used to highlight operators..")
;;
;;;;(defface font-lock-RTT-c++-number-face
;;;;  '((((class color))
;;;;     (:inherit font-lock-builtin-face :bold t)))
;;;;  "Font Lock mode face used to highlight operators..")

;;;(defface font-lock-RTT-c++-number-face
;;;  '((((class color))
;;;       (:background "darkseagreen2")))
;;;  "Basic face for highlighting.")
;;(defface font-lock-RTT-c++-number-face
;;  '((((class color))
;;       (:inherit font-lock-string-face)))
;;  "Basic face for highlighting.")

;shite (font-lock-add-keywords 'c++-mode
;shite   '(("\\(~^<>:=,.\\+*/%-]\\)" 0 'font-lock-c++-operator-face)))


(defface font-lock-rtt-1-face
  '((((class color))
     (:inherit font-lock-variable-name-face :bold 0 :background "darkseagreen2")))
  "RTT colour 1")

(defface font-lock-rtt-2-face
  '((((class color))
     (:inherit font-lock-variable-name-face :background "black" :foreground "chocolate1")))
  "RTT colour 2")

(defface rtt-new-face-1-face
  '((((class color) (min-colors 88) (background dark))
     :background "black" :foreground "red"))
  "rtts peens")



(defface rtt-face-1
  '((t (:foreground "SandyBrown" :bold t)))
  "rtt1"
  :group 'rtt-faces)
(defvar rtt-face-1 'rtt-face-1)

(defface rtt-face-2
  '((t (:foreground "khaki" :bold t)))
  "rtt1"
  :group 'rtt-faces)
(defvar rtt-face-2 'rtt-face-2)

(defface rtt-c++-round-brackets-face
  '((((class color) (min-colors 88) (background dark))
     :background "black" :foreground "LightSalmon"))
  "rtts c++ round brackets face")
(defvar rtt-c++-round-brackets-face 'rtt-c++-round-brackets-face)


(add-hook 'c-mode-common-hook (lambda ()
    (font-lock-add-keywords nil '(

        ; Valid hex number (will highlight invalid suffix though)
        ("\\b0x[[:xdigit:]]+[uUlL]*\\b" . font-lock-constant-face)

        ; Invalid hex number
;        ("\\b0x\\(\\w\\|\\.\\)+\\b" . font-lock-warning-face)

        ; Valid floating point number.
        ("\\(\\b[0-9]+\\|\\)\\(\\.\\)\\([0-9]+\\(e[-]?[0-9]+\\)?\\([lL]?\\|[dD]?[fF]?\\)\\)\\b" (1 font-lock-constant-face) (3 font-lock-constant-face))

        ; Invalid floating point number.  Must be before valid decimal.
;        ("\\b[0-9].*?\\..+?\\b" . font-lock-warning-face)

        ; Valid decimal number.  Must be before octal regexes otherwise 0 and 0l
        ; will be highlighted as errors.  Will highlight invalid suffix though.
        ("\\b\\(\\(0\\|[1-9][0-9]*\\)[uUlL]*\\)\\b" 1 font-lock-constant-face)

        ; Valid octal number
        ("\\b0[0-7]+[uUlL]*\\b" . font-lock-constant-face)

        ; RTTTTTT brackets
        ("[()]" . rtt-c++-round-brackets-face)

        ; RTTTTTT
        ("[;:]" . rtt-face-1)

        ; RTTTTTT misc chars
        ("[-+,<>.]" . font-lock-doc-face)

        ; RTTTTTT misc chars
        ("=" . rtt-face-2)

        ; Floating point number with no digits after the period.  This must be
        ; after the invalid numbers, otherwise it will "steal" some invalid
        ; numbers and highlight them as valid.
        ("\\b\\([0-9]+\\)\\." (1 font-lock-constant-face))

        ; Invalid number.  Must be last so it only highlights anything not
        ; matched above.
;        ("\\b[0-9]\\(\\w\\|\\.\\)+?\\b" . font-lock-warning-face)
    ))
    ))



(setq blink-cursor-interval .067)
;(setq blink-cursor-interval .03)
(setq blink-cursor-blinks 1000)
(setq blink-cursor-delay 0.2)

(delete-selection-mode 1)

; suppress the XXXXXX has a running process; kill it?has a running process; kill it? 
(defun set-no-process-query-on-exit ()
  (let ((proc (get-buffer-process (current-buffer))))
    (when (processp proc)
      (set-process-query-on-exit-flag proc nil))))

;(add-hook 'term-exec-hook 'set-no-process-query-on-exit)
;(add-hook 'gud-mode-hook 'set-no-process-query-on-exit) ;for gdb

(set-no-process-query-on-exit) ; // for all emacs

; suppress querying to kill a buffer
(setq kill-buffer-query-functions (delq 'process-kill-buffer-query-function kill-buffer-query-functions))

(defun RTT-disable-autocompleting-inside-GUD ()
  "cos goes mad on freebsd"
  (auto-complete-mode 0)
  (company-mode 0)
)

(add-hook 'gud-mode-hook 'RTT-disable-autocompleting-inside-GUD)


;(require 'imenu)
;(require 'speedbar)
;(add-hook 'speedbar-mode-hook
;          (lambda ()
;            (speedbar-change-initial-expansion-list "Sb-Imenu")))
;(sb-imenu-install-speedbar-variables)
;(setq speedbar-initial-expansion-list-name "Sb-Imenu")
;(speedbar 1)

;                        _                  ___ ____  _____
;    ___ _ __ ___   __ _| | _____          |_ _|  _ \| ____|
;   / __| '_ ` _ \ / _` | |/ / _ \  _____   | || | | |  _|
;  | (__| | | | | | (_| |   <  __/ |_____|  | || |_| | |___
;   \___|_| |_| |_|\__,_|_|\_\___|         |___|____/|_____|
;                                                           

(cmake-ide-setup)


;                           _ _                
;   ___ _ __   ___  ___  __| | |__   __ _ _ __ 
;  / __| '_ \ / _ \/ _ \/ _` | '_ \ / _` | '__|
;  \__ \ |_) |  __/  __/ (_| | |_) | (_| | |   
;  |___/ .__/ \___|\___|\__,_|_.__/ \__,_|_|   
;      |_|
;  

(require 'speedbar)

(setq speedbar-initial-expansion-list-name "sb-imenu")

;                           _ _                                 _
;   ___ _ __   ___  ___  __| | |__   __ _ _ __   ___  ___  _ __| |_
;  / __| '_ \ / _ \/ _ \/ _` | '_ \ / _` | '__| / __|/ _ \| '__| __|
;  \__ \ |_) |  __/  __/ (_| | |_) | (_| | |    \__ \ (_) | |  | |_
;  |___/ .__/ \___|\___|\__,_|_.__/ \__,_|_|    |___/\___/|_|   \__|
;      |_|
;  

(require 'speedbar)

;(defun speedbar-buffer-buttons-engine (temp)
;  "Create speedbar buffer buttons.
;If TEMP is non-nil, then clicking on a buffer restores the previous display."
;  (speedbar-insert-separator "Active Buffers:")
;  (let* ((bl (buffer-list))
;         (abl ;; alphabetized buffer-list
;           (sort
;              bl
;              #'(lambda (e1 e2)
;                  (string-lessp (buffer-name e1) (buffer-name e2)))))
;          (case-fold-search t))
;    (while abl
;      (if (string-match "^[ *]" (buffer-name (car abl)))
;    nil
;  (let* ((known (string-match speedbar-file-regexp
;            (buffer-name (car abl))))
;         (expchar (if known ?+ ??))
;         (fn (if known 'speedbar-tag-file nil))
;         (fname (with-current-buffer (car abl)
;                        (buffer-file-name))))
;    (speedbar-make-tag-line 'bracket expchar fn
;          (if fname (file-name-nondirectory fname))
;          (buffer-name (car abl))
;          'speedbar-buffer-click temp
;          'speedbar-file-face 0)
;    (speedbar-buffers-tail-notes (car abl))))
;      (setq abl (cdr abl)))
;    (setq bl (buffer-list)
;          abl ;; alphabetized buffer-list
;           (sort
;              bl
;              #'(lambda (e1 e2)
;                  (string-lessp (buffer-name e1) (buffer-name e2)))))
;    (speedbar-insert-separator "Scratch Buffers:")
;    (while abl
;      (if (not (string-match "^\\*" (buffer-name (car abl))))
;    nil
;  (if (eq (car abl) speedbar-buffer)
;      nil
;    (speedbar-make-tag-line 'bracket ?? nil nil
;          (buffer-name (car abl))
;          'speedbar-buffer-click temp
;          'speedbar-file-face 0)
;    (speedbar-buffers-tail-notes (car abl))))
;      (setq abl (cdr abl)))
;    ;; Need to research why this last line is needed (if at all)?
;    (setq bl (buffer-list))))

(provide 'init)
;;; init.el ends here

