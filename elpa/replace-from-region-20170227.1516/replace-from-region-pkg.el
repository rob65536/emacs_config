;;; -*- no-byte-compile: t -*-
(define-package "replace-from-region" "20170227.1516" "Replace commands whose query is from region" 'nil :commit "dc9318b9b2822da7b00ecc34d1dc965c8f96c9bb" :url "http://www.emacswiki.org/emacs/download/replace-from-region.el" :keywords '("replace" "search" "region"))
