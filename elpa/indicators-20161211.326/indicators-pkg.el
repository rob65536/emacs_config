;;; -*- no-byte-compile: t -*-
(define-package "indicators" "20161211.326" "Display the buffer relative location of line in the fringe." '((dash "2.13.0") (cl-lib "0.5.0")) :commit "f62a1201f21453e3aca93f48483e65ae8251432e" :url "https://github.com/Fuco1/indicators.el" :keywords '("fringe" "frames"))
