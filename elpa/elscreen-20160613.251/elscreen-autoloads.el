;;; elscreen-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "elscreen" "elscreen.el" (22818 3077 185796
;;;;;;  400000))
;;; Generated autoloads from elscreen.el

(autoload 'elscreen-start "elscreen" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("elscreen-buffer-list.el" "elscreen-color-theme.el"
;;;;;;  "elscreen-dired.el" "elscreen-dnd.el" "elscreen-gf.el" "elscreen-goby.el"
;;;;;;  "elscreen-howm.el" "elscreen-pkg.el" "elscreen-server.el"
;;;;;;  "elscreen-speedbar.el" "elscreen-w3m.el" "elscreen-wl.el")
;;;;;;  (22818 3077 209810 600000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; elscreen-autoloads.el ends here
