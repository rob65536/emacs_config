;;; -*- no-byte-compile: t -*-
(define-package "company-rtags" "20170924.2244" "RTags back-end for company" '((emacs "24.3") (company "0.8.1") (rtags "2.10")) :commit "2b0c88cc470b06b65232c23df4b6fbfc4b534580" :url "http://rtags.net")
